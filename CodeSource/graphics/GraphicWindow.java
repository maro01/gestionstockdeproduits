package WindowBuilder.graphics;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import WindowBuilder.poo.*;

import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.util.Iterator;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;
import javax.swing.ImageIcon;
import javax.swing.UIManager;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.Color;
import java.awt.Font;

public class GraphicWindow extends JFrame {

	private JPanel contentPane;
	private JTextField textCodeArticle;
	private JTextField textDescriptionArticle;
	private JTextField textPrixArticle;
	private JTextField textCodeMagasin;
	private JTextField textDescriptionMagasin;
	private JLabel lblQuantit;
	private JTextField textQuantit�;

	
	/////////////////////////////////////////////////
	protected Article article=new Article();
	protected Magasin magasin=new Magasin();
	protected Stock stock=new Stock();
	int acces=0
			;
	private JButton btnExit;
	private JFrame sortie =new JFrame("Sortie");
	
	 String outputArticleDescription;
	 double outputQuantit�;
	 double outputPrix;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (Throwable e) {
			e.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GraphicWindow frame = new GraphicWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	
	public GraphicWindow() {
		setTitle("frame");
		initComponents();
		initEvents();
		
	}
////////////Initialiser et cr�er les JComponents
	private void initComponents() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(GraphicWindow.class.getResource("/WindowBuilder/ressources/Icon_128.ico")));
		setTitle("Gestion des stocks");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 446);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		setUndecorated(true);
		JLabel lblCodeDarticle = new JLabel("Code d'article");
		JLabel Error = new JLabel("");
		Error.setForeground(Color.RED);
		Error.setText(null);
		
		textCodeArticle = new JTextField();
		textCodeArticle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try
				{
					
			String input =textCodeArticle.getText();
			
			article.setCode_article(input);
			
			if(input.isEmpty())  {
				Error.setText("Voyez vos cases s'il vous plait !");
				acces=0;
			                     }
			if(!input.isEmpty())
			{
				Error.setText(null);
				acces=1;
			}
			
				}
			catch(Exception exeption)
			{
				Error.setText("Voyez vos cases s'il vous plait !");
				
			}
			}
		});
		textCodeArticle.setColumns(10);
		
		JLabel lblDescriptionDarticle = new JLabel("Description d'article");
		
		textDescriptionArticle = new JTextField();
		textDescriptionArticle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try
				{
					
				String input = textDescriptionArticle.getText();
				article.setDescription(input);
				if(input.isEmpty())  {
					Error.setText("Voyez vos cases s'il vous plait !");
					acces=0;
				                     }
				if(!input.isEmpty())
				{
					Error.setText(null);
					acces=1;
				}
				}
				catch(Exception exeption)
				{
					Error.setText("Voyez vos cases s'il vous plait !");
					
				}
			}
		});
		textDescriptionArticle.setColumns(10);
		
		JLabel lblPrixDarticle = new JLabel("Prix d'article");
		
		textPrixArticle = new JTextField();
		textPrixArticle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try
				{
					
				String input =textPrixArticle.getText();
				article.setPrix(Double.parseDouble(input));
				if(input.isEmpty())  {
					Error.setText("Voyez vos cases s'il vous plait !");
					acces=0;
				                     }
				if(!input.isEmpty())
				{
					Error.setText(null);
					acces=1;
				}
				}
				catch(Exception exeption)
				{
					Error.setText("Voyez vos cases s'il vous plait !");
					
				}
			}});
		textPrixArticle.setColumns(10);
		
		JLabel lblCodeMagasin = new JLabel("Code Magasin");
		JLabel lblCalcul = new JLabel("");
		textCodeMagasin = new JTextField();
		textCodeMagasin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try
				{
					
				String input =textCodeMagasin.getText();
				magasin.setCode_magasin(input);
				if(input.isEmpty())  {
					Error.setText("Voyez vos cases s'il vous plait !");
					acces=0;
				                     }
				if(!input.isEmpty())
				{
					Error.setText(null);
					acces=1;
				}
				}
				catch(Exception exeption)
				{
					Error.setText("Voyez vos cases s'il vous plait !");
					
				}
			}
		});
		textCodeMagasin.setColumns(10);
		
		JLabel lblDescriptionMagasin = new JLabel("Description Magasin");
		
		textDescriptionMagasin = new JTextField();
		textDescriptionMagasin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try
				{
					
				String input = textDescriptionMagasin.getText();
				magasin.setDescription(input);
				if(input.isEmpty())  {
					Error.setText("Voyez vos cases s'il vous plait !");
					acces=0;
				                     }
				if(!input.isEmpty())
				{
					Error.setText(null);
					acces=1;
				}
				}
				catch(Exception exeption)
				{
					Error.setText("Voyez vos cases s'il vous plait !"); 
					acces=0;
				}
			}
		});
		textDescriptionMagasin.setColumns(10);
		
		lblQuantit = new JLabel("Quantit\u00E9");
		
		textQuantit� = new JTextField();
		textQuantit�.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try
				{
					
				String input =textQuantit�.getText();
				double double_input=Double.parseDouble(input);
				stock.setQte(double_input);
				if(input.isEmpty())  {
					Error.setText("Voyez vos cases s'il vous plait !");
					acces=0;
				                     }
				if(!input.isEmpty())
				{
					Error.setText(null);
					acces=1;
				}
				}
				catch(Exception exeption)
				{
					Error.setText("Voyez vos cases s'il vous plait !");
				}
			}
		});
		textQuantit�.setColumns(10);
		
		JButton btnRefresh = new JButton("R\u00E9initialiser");
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
				
				textCodeArticle.setText(null);
				textDescriptionArticle.setText(null);
				textPrixArticle.setText(null);
				textCodeMagasin.setText(null);
				textDescriptionMagasin.setText(null);
				textQuantit�.setText(null);
				lblCalcul.setText(null);
				Error.setText(null);
				
			
			}
		});
		
		btnExit = new JButton("Quitter");
		btnExit.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				int confirmation = JOptionPane.showConfirmDialog(null, "Vous etes s�re que vous voulez quitter","Confirmation",JOptionPane.YES_NO_OPTION);
				if(confirmation == JOptionPane.YES_NO_OPTION)
				System.exit(0);
			}

			
		});
		
		JButton btnMenu = new JButton("Se deconnecter");
		btnMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int confirmation = JOptionPane.showConfirmDialog(null, "Vous voulez d�connectez de votre compte ?", "Deconnexion",JOptionPane.YES_NO_OPTION);
				if(JOptionPane.YES_NO_OPTION==confirmation)
				{
					dispose();
					LoginSystem info =new LoginSystem();
					info.Login.setVisible(true);
				}
			}
		});
		
		
		
		lblCalcul.setFont(new Font("Tahoma", Font.PLAIN, 12));
		JButton btnCalculer = new JButton("Calculer");
		btnCalculer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try
				{
					if(!textCodeArticle.getText().isEmpty() && !textDescriptionArticle.getText().isEmpty() && !textPrixArticle.getText().isEmpty() && !textCodeMagasin.getText().isEmpty() && !textDescriptionMagasin.getText().isEmpty()
							&& !textQuantit�.getText().isEmpty())
						acces=1;
					else
					{
						Error.setText("Voyez vos cases s'il vous plait !");
						acces=0;
					}
				lblCalcul.setForeground(Color.BLACK);
			 outputArticleDescription=textDescriptionArticle.getText();
			 outputQuantit�=Double.parseDouble(textQuantit�.getText());
			 outputPrix=Double.parseDouble(textPrixArticle.getText());
		
			if(acces==1)
			{
				Error.setText(null);
				article.setCode_article(textCodeArticle.getText());
				article.setDescription(textDescriptionArticle.getText());
				article.setPrix(Double.parseDouble(textPrixArticle.getText()));
				magasin.setCode_magasin(textCodeMagasin.getText());
				magasin.setDescription(textDescriptionMagasin.getText());
			    stock.setQte(Double.parseDouble(textPrixArticle.getText()));
			    if(Error!=null)
				lblCalcul.setText("La valeur du stock de l'article "+outputArticleDescription+" est "+outputQuantit�*outputPrix+" DH");
			}
			if(acces==0) 
			{
	        Error.setText("Voyez vos cases s'il vous plait !");
	        lblCalcul.setText(null);
			}
			}
				catch(Exception e)
				{
					Error.setText("Voyez vos cases s'il vous plait !");
				}
			}	
		});
		btnCalculer.setFont(new Font("Verdana", Font.PLAIN, 16));
		
		JButton btnSauvegarder = new JButton("Sauvegarder");
		btnSauvegarder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					String filename = JOptionPane.showInputDialog(null, "Nom du fichier");
					if(filename==null) JOptionPane.showMessageDialog(null,"Le nom du fichier est vide");
					if(filename!=null)
					{
						
					FileWriter data = new FileWriter("Sauvegarde d'application/"+filename+".txt");
			
					data.write("Code d'article :"+article.getCode_article());
					 data.write(System.getProperty( "line.separator" ));
					data.write("Description :"+article.getDescription());
					 data.write(System.getProperty( "line.separator" ));
				    data.write("Prix :"+article.getPrix()+"\n");
				    data.write(System.getProperty( "line.separator" ));
				    data.write("-----------------------------------");
				    data.write(System.getProperty( "line.separator" ));
				    data.write("Code du magasin :"+magasin.getCode_magasin());
				    data.write(System.getProperty( "line.separator" ));
					data.write("Description :"+magasin.getDescription());
					 data.write(System.getProperty( "line.separator" ));
				    data.write("Prix :"+article.getPrix());
				    data.write(System.getProperty( "line.separator" ));
				    data.write("-----------------------------------");
				    data.write(System.getProperty( "line.separator" ));
				    data.write("Quantit� du stock :"+stock.getQte());
				    data.write(System.getProperty( "line.separator" ));
				    data.write("-----------------------------------");
				    data.write("La valeur du stock de l'article "+outputArticleDescription+" est "+outputQuantit�*outputPrix+" DH");
	             	data.write(System.getProperty( "line.separator" ));
		          
				    data.close();
					}   
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		
			}
		});
		
	
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addContainerGap()
							.addComponent(btnRefresh, GroupLayout.PREFERRED_SIZE, 112, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(btnMenu, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED, 11, Short.MAX_VALUE)
							.addComponent(btnExit, GroupLayout.PREFERRED_SIZE, 122, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
								.addComponent(lblDescriptionDarticle)
								.addComponent(lblCodeDarticle)
								.addComponent(lblPrixDarticle)
								.addComponent(lblCodeMagasin)
								.addComponent(lblDescriptionMagasin, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(lblQuantit))
							.addGap(25)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addComponent(textQuantit�, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addGap(206))
								.addGroup(gl_contentPane.createSequentialGroup()
									.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
										.addComponent(textDescriptionMagasin, Alignment.LEADING)
										.addComponent(textCodeMagasin, Alignment.LEADING)
										.addComponent(textPrixArticle, Alignment.LEADING)
										.addComponent(textDescriptionArticle, Alignment.LEADING)
										.addComponent(textCodeArticle, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 213, Short.MAX_VALUE))
									.addGap(79))))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(101)
							.addComponent(btnCalculer, GroupLayout.PREFERRED_SIZE, 218, GroupLayout.PREFERRED_SIZE))
						.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
							.addContainerGap()
							.addComponent(lblCalcul, GroupLayout.PREFERRED_SIZE, 377, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap())
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(Error, GroupLayout.PREFERRED_SIZE, 242, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 66, Short.MAX_VALUE)
					.addComponent(btnSauvegarder)
					.addGap(21))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnExit)
						.addComponent(btnRefresh)
						.addComponent(btnMenu))
					.addGap(35)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblCodeDarticle)
						.addComponent(textCodeArticle, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblDescriptionDarticle)
						.addComponent(textDescriptionArticle, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(26)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblPrixDarticle)
						.addComponent(textPrixArticle, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblCodeMagasin)
						.addComponent(textCodeMagasin, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblDescriptionMagasin)
						.addComponent(textDescriptionMagasin, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(19)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblQuantit)
						.addComponent(textQuantit�, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(Error, GroupLayout.PREFERRED_SIZE, 13, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnSauvegarder))
					.addGap(23)
					.addComponent(lblCalcul, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
					.addComponent(btnCalculer))
		);
		contentPane.setLayout(gl_contentPane);
		
	}
protected int JOptionPane(JFrame sortie2, String string, String string2, int yesNoOption) {
	// TODO Auto-generated method stub
	return 0;
}

////////////Creer les events
	private void initEvents() {
		// TODO Auto-generated method stub
		
	}
}
