package WindowBuilder.graphics;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.BorderLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;

import WindowBuilder.graphics.GraphicWindow;

import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;
import javax.swing.UIManager;

public class LoginSystem {

	 JFrame Login;
	private JTextField UsernameText;
	private JPasswordField PasswordText;
	int option_pane_helper=0;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (Throwable e) {
			e.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginSystem window = new LoginSystem();
					window.Login.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public LoginSystem() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		Login = new JFrame();
		Login.setTitle("Gestion des stocks");
		Login.setIconImage(Toolkit.getDefaultToolkit().getImage(LoginSystem.class.getResource("/WindowBuilder/ressources/Icon_128.ico")));
		Login.setBounds(100, 100, 450, 300);
		Login.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Login.setUndecorated(true);
		
		JLabel LoginLabel = new JLabel("Login");
		LoginLabel.setFont(new Font("Trebuchet MS", Font.PLAIN, 40));
		
		UsernameText = new JTextField();
		UsernameText.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
			}
		});
		UsernameText.setColumns(10);
		
		JLabel UsernameLabel = new JLabel("Nom d'utilisateur");
		UsernameLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		JLabel PasswordLabel = new JLabel("Mot de passe");
		PasswordLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		PasswordText = new JPasswordField();
		GraphicInscription info =new GraphicInscription();
		
		JButton btnValider = new JButton("Valider");
		btnValider.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				option_pane_helper=0;
				String user=UsernameText.getText();
				String password=PasswordText.getText(); // Protected
				String usr_file =null;
				String password_file=null;

				    try
				{
					 usr_file = new String(Files.readAllBytes(Paths.get("Sauvegarde d'application/Securite/user.txt")), "UTF-8");
					 password_file = new String(Files.readAllBytes(Paths.get("Sauvegarde d'application/Securite/password.txt")), "UTF-8");
				}
				catch(Exception exept) {}
			
					  if(user.equals(usr_file) && password.equals(password_file))
						{
							UsernameText.setText(null);
							PasswordText.setText(null);
							
							Login.dispose();
							GraphicWindow application =new GraphicWindow();
							application.setVisible(true);

						} 
		
				if(!user.equals(usr_file) && !password.equals(password_file) && option_pane_helper==0)
				 {
				 JOptionPane.showMessageDialog(Login, "Username and Password are wrong","Login", JOptionPane.ERROR_MESSAGE);
				 UsernameText.setText(null);
					PasswordText.setText(null);
					option_pane_helper++;
				 }
			 
			 if(!user.equals(usr_file) && option_pane_helper==0) 
			 {
				 JOptionPane.showMessageDialog(Login, "Username is wrong","Login",JOptionPane.ERROR_MESSAGE);
				 UsernameText.setText(null);
					PasswordText.setText(null);
					option_pane_helper++;
			 }
			 if(!password.equals(password_file) && option_pane_helper==0)
				 {
				 JOptionPane.showMessageDialog(Login, "Password is wrong","Login",JOptionPane.ERROR_MESSAGE);
				 UsernameText.setText(null);
					PasswordText.setText(null);
					option_pane_helper++;
				 }
				
				
				
				 
				 }
	
		});
		
		JButton btnExit = new JButton("Retourner");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Login.dispose();
				GraphicMain appmain =new GraphicMain();
				appmain.Main.setVisible(true);
					
			
			}
		});
		GroupLayout groupLayout = new GroupLayout(Login.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap(77, Short.MAX_VALUE)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
							.addComponent(LoginLabel, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)
							.addGap(109))
						.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(UsernameLabel)
								.addComponent(PasswordLabel))
							.addGap(18)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
								.addComponent(PasswordText)
								.addComponent(UsernameText, GroupLayout.DEFAULT_SIZE, 168, Short.MAX_VALUE)
								.addGroup(Alignment.TRAILING, groupLayout.createParallelGroup(Alignment.LEADING, false)
									.addComponent(btnExit, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
									.addComponent(btnValider, GroupLayout.DEFAULT_SIZE, 75, Short.MAX_VALUE)))
							.addGap(81))))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(LoginLabel, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
					.addGap(28)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(UsernameText, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(UsernameLabel))
					.addGap(26)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(PasswordLabel)
						.addComponent(PasswordText, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(btnValider)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(btnExit)
					.addContainerGap(17, Short.MAX_VALUE))
		);
		Login.getContentPane().setLayout(groupLayout);
		
	}

	protected int JOptionPane(Object object, String string, String string2, int yesNoOption) {
		// TODO Auto-generated method stub
		return 0;
	}
	
}
