package WindowBuilder.graphics;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.UIManager;
import java.awt.Toolkit;



public class GraphicInscription {

	 JFrame frmGestionDesStocks;
	private JTextField textPrenom;
	private JTextField textNom;
	private JTextField textEmail;
	private JTextField textPassword;
	private JTextField textConfirmPassword;
	private JTextField textUsername;
	private static String username;
	private static String password;
	static int inscrit=0;
	static int validate=0;
	static int goodpass=0;
	static int goodemail=0;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (Throwable e) {
			e.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GraphicInscription window = new GraphicInscription();
					window.frmGestionDesStocks.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GraphicInscription() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmGestionDesStocks = new JFrame();
		frmGestionDesStocks.setIconImage(Toolkit.getDefaultToolkit().getImage(GraphicInscription.class.getResource("/WindowBuilder/ressources/Icon_128.ico")));
		frmGestionDesStocks.setTitle("Gestion des stocks");
		frmGestionDesStocks.setBounds(100, 100, 450, 396);
		frmGestionDesStocks.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmGestionDesStocks.setUndecorated(true);		
		JLabel lblNewCompte = new JLabel("Cr\u00E9ez un compte");
		lblNewCompte.setFont(new Font("Tahoma", Font.PLAIN, 20));
		
		JLabel lblPrenom = new JLabel("Pr\u00E9nom");
		lblPrenom.setFont(new Font("Tahoma", Font.PLAIN, 16));
		JLabel lblError = new JLabel("");
		lblError.setForeground(Color.RED);
		textPrenom = new JTextField();
		textPrenom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try
				{
					 if(textPrenom.getText().isEmpty()) 
					 {
						 lblError.setText("Votre saisie est incorrect !"); 
					 }
					 else 
					 {
						 lblError.setText(null);
						 
					 }
				}
				catch(Exception e)
				{
					lblError.setText("Votre saisie est incorrect !");
				}
			}
		});
		textPrenom.setColumns(10);
		
		JLabel lblNom = new JLabel("Nom");
		lblNom.setFont(new Font("Tahoma", Font.PLAIN, 16));
		JLabel lblGoodpass = new JLabel("");
		lblGoodpass.setForeground(Color.RED);
		
		JLabel lblEmail = new JLabel("E-mail");
		lblEmail.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		JLabel lblPassword = new JLabel("Mot de passe");
		lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		textNom = new JTextField();
		textNom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try
				{
					 if(textNom.getText().isEmpty()) 
					 {
						 lblError.setText("Votre saisie est incorrect !"); 
					 }
					 else 	
					 {
						 lblError.setText(null);
						
					 }
				}
				catch(Exception e1)
				{
					lblError.setText("Votre saisie est incorrect !");
				}
			}
		});
		textNom.setColumns(10);
		
		textEmail = new JTextField();
		textEmail.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e3) {
				try
				{
					 String CorrectEmail = 
						    "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
						    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

						if (!textEmail.getText().matches(CorrectEmail)) {
							lblError.setText(null);
						}
						else
					 {
						 lblError.setText("Votre saisie est incorrect !"); 
					 }
						 
				}
				catch(Exception e4)
				{
					lblError.setText("Votre saisie est incorrect !");
				}
			}
		});
		textEmail.setColumns(10);
		
		textPassword = new JTextField();
		textPassword.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try
				{
					 if(textPassword.getText().isEmpty()) 
					 {
						 lblError.setText("Votre saisie est incorrect !"); 
					 }
					 else 
					 {
						 lblError.setText(null);
						
					 }
				}
				catch(Exception e5)
				{
					lblError.setText("Votre saisie est incorrect !");
				}
			}
		});
		textPassword.setColumns(10);
		
		JLabel lblConfirmPassword = new JLabel("Confirmer le mot de passe");
		lblConfirmPassword.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		textConfirmPassword = new JTextField();
		textConfirmPassword.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try
				{
					 if(textConfirmPassword.getText().isEmpty()) 
					 {
						 lblError.setText("Votre saisie est incorrect !"); 
					 }
					 else 
					 {
						 lblError.setText(null);
						
					 }
						 
				}
				catch(Exception e6)
				{
					lblError.setText("Votre saisie est incorrect !");
				}
			}
		});
		textConfirmPassword.setColumns(10);
		
		JButton btnValider = new JButton("Valider");
	
		btnValider.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(textPassword.getText().length()>=4)
				{
					goodpass=1;
					lblGoodpass.setText(null);
					System.out.println("1");
				}
				if(textPassword.getText().length()<4)
				{
					if(lblError.getText()==null)
					lblGoodpass.setText("Le mot de passe doit contenir 4 caract�res ou plus");
					
				System.out.println("0");
				}
					
				if(textEmail.getText().contains("@gmail.com") || textEmail.getText().contains("@hotmail.com") || textEmail.getText().contains("@outlook.com") )
				{
					goodemail=1;
					lblError.setText(null);
				}
				if(!textEmail.getText().contains("@gmail.com") && !textEmail.getText().contains("@hotmail.com") && !textEmail.getText().contains("@outlook.com") )
					lblError.setText("Email est invalide");
				
				if(!textPrenom.getText().isEmpty() && !textNom.getText().isEmpty() && !textUsername.getText().isEmpty()
						&& !textEmail.getText().isEmpty() && !textPassword.getText().isEmpty() && !textConfirmPassword.getText().isEmpty())
				{
					validate=1;
				}
				if(textPrenom.getText().isEmpty() || textNom.getText().isEmpty() || textUsername.getText().isEmpty()
						|| textEmail.getText().isEmpty() || textPassword.getText().isEmpty() || textConfirmPassword.getText().isEmpty())
				{
					lblError.setText("Voyez vos cases s'il vous plait");
				}
				if( !textPassword.getText().equals(textConfirmPassword.getText()))
				{
					lblError.setText(null);
					lblError.setText("Voyez votre mot de passe");
				}
				
				if( textPassword.getText().equals(textConfirmPassword.getText()) && goodpass==1   && validate>0 && goodemail==1 )
				{
					lblError.setText(null);
				inscrit++;
				setUsername(textUsername.getText());
				setPassword(textPassword.getText());
				
				
				String nom=textNom.getText();
				String prnm=textPrenom.getText();
				 username=textUsername.getText();
				String email=textEmail.getText();
				 password=textPassword.getText();
				String confirmpassword=textConfirmPassword.getText();
				FileWriter data =null;
				FileWriter Username=null;
				FileWriter Password=null;
				try
				{
					File dir1 = new File("Sauvegarde d'application");
					dir1.mkdirs();
					
					File dir2 = new File("Sauvegarde d'application/Securite");
					dir2.mkdirs();			
					
				    

					
			        File datafile = new File(dir2, "Data.txt");datafile.createNewFile();
			        File userfile = new File(dir2, "user.txt");userfile.createNewFile();
			        File passfile = new File(dir2, "password.txt");passfile.createNewFile();
			        Path path = Paths.get("Sauvegarde d'application/Securite/");
			        Files.setAttribute(path, "dos:hidden", true);
			        
			    data = new FileWriter("Sauvegarde d'application/Securite/Data.txt");
			    Username= new FileWriter("Sauvegarde d'application/Securite/user.txt");
			    Password =new FileWriter("Sauvegarde d'application/Securite/password.txt");
			    
		        Path path1 = Paths.get("Sauvegarde d'application/Securite/Data.txt");
		        Files.setAttribute(path1, "dos:hidden", true);
		        Path path2 = Paths.get("Sauvegarde d'application/Securite/user.txt");
		        Files.setAttribute(path2, "dos:hidden", true);
		        Path path3 = Paths.get("Sauvegarde d'application/Securite/password.txt");
		        Files.setAttribute(path3, "dos:hidden", true);

				data.write("Nom complet : "+nom+" "+prnm);
				data.write(System.getProperty( "line.separator" ));
                data.write("Email     : "+email);
				Password.write(password);
				Username.write(username);
				data.close();
				Password.close();
				Username.close();
		  
				}
				catch(Exception dataproblem)
				{
					
				}
                  
				frmGestionDesStocks.dispose();
				GraphicMain info=new GraphicMain();
				info.Main.setVisible(true);
				
				
			   }
				

			}

			private void setHiddenAttrib(File file) throws InterruptedException {
				try {
				      // execute attrib command to set hide attribute
				      Process p = Runtime.getRuntime().exec("attrib +H " + file.getPath());
				      // for removing hide attribute
				      //Process p = Runtime.getRuntime().exec("attrib -H " + file.getPath());
				      p.waitFor(); 
				      if(file.isHidden()) {
				        System.out.println(file.getName() + " hidden attribute is set to true");
				      }else {
				        System.out.println(file.getName() + " hidden attribute not set to true");
				      }
				    } catch (IOException e) {
				      ((Throwable) e).printStackTrace();
				    }
			}
		});
		
		
		
		JButton btnReinitialiser = new JButton("Reinitialiser");
		btnReinitialiser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textPrenom.setText(null);
				textNom.setText(null);
				textEmail.setText(null);
				textPassword.setText(null);
				textConfirmPassword.setText(null);
				textUsername.setText(null);
				
			}
		});
		
		JButton btnQuitter = new JButton("Retourner");
		btnQuitter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int confirmation= JOptionPane.showConfirmDialog(null, "Vous �tes s�r que vous voulez quitter ?","Confirmation",JOptionPane.YES_NO_OPTION);
				if(confirmation==JOptionPane.YES_NO_OPTION)
				{
					frmGestionDesStocks.dispose();
					GraphicMain info=new GraphicMain();
					info.Main.setVisible(true);
				}
			}
		});
		
		JLabel lblUsername = new JLabel("Nom d'utilisateur");
		lblUsername.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		
		
		
		textUsername = new JTextField();
		textUsername.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try
				{
					 if(textPrenom.getText().isEmpty()) 
					 {
						 lblError.setText("Votre saisie est incorrect !"); 
					 }
					 else
					 {
						 lblError.setText(null);
						
						 
					 }
				}
				catch(Exception e2)
				{
					lblError.setText("Votre saisie est incorrect !");
				}
			}
		});
		textUsername.setColumns(10);
		

		
		
		GroupLayout groupLayout = new GroupLayout(frmGestionDesStocks.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(137)
					.addComponent(lblNewCompte)
					.addContainerGap(163, Short.MAX_VALUE))
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblConfirmPassword)
						.addComponent(lblPassword, GroupLayout.PREFERRED_SIZE, 127, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblEmail, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(253, Short.MAX_VALUE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(lblPrenom)
										.addComponent(lblNom, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
									.addGap(213))
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(lblUsername, GroupLayout.DEFAULT_SIZE, 245, Short.MAX_VALUE)
									.addGap(33)))
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(textUsername, GroupLayout.PREFERRED_SIZE, 142, GroupLayout.PREFERRED_SIZE)
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
									.addComponent(textNom)
									.addComponent(textPrenom, GroupLayout.DEFAULT_SIZE, 142, Short.MAX_VALUE))
								.addComponent(textEmail, 144, 144, 144)
								.addComponent(textPassword, 144, 144, 144)
								.addComponent(textConfirmPassword, GroupLayout.PREFERRED_SIZE, 142, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED, 0, Short.MAX_VALUE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(76)
							.addComponent(btnValider, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnReinitialiser, GroupLayout.PREFERRED_SIZE, 108, GroupLayout.PREFERRED_SIZE)))
					.addGap(14))
				.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
					.addContainerGap(310, Short.MAX_VALUE)
					.addComponent(btnQuitter, GroupLayout.PREFERRED_SIZE, 108, GroupLayout.PREFERRED_SIZE)
					.addGap(32))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(52)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblGoodpass, GroupLayout.PREFERRED_SIZE, 347, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblError, GroupLayout.PREFERRED_SIZE, 347, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(51, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(21)
					.addComponent(lblNewCompte)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblPrenom)
						.addComponent(textPrenom, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNom, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
						.addComponent(textNom, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(13)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblUsername, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
						.addComponent(textUsername, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblEmail, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
						.addComponent(textEmail, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblPassword, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
						.addComponent(textPassword, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(15)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblConfirmPassword, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
						.addComponent(textConfirmPassword, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(31)
					.addComponent(lblError, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblGoodpass, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE)
					.addGap(4)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
							.addComponent(btnReinitialiser)
							.addComponent(btnValider))
						.addComponent(btnQuitter))
					.addContainerGap())
		);
		frmGestionDesStocks.getContentPane().setLayout(groupLayout);
	}
	static public String getUsername()
		{
			return username;
		}
	static public   String getPassword()
		{
			return password;
		}
	 protected void setUsername(String user)
	 {
		 username=user;
	 }
	 protected void setPassword(String pass)
	 {
		password=pass;
	 }
}


