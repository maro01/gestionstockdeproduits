package WindowBuilder.poo;


public class Article {
	
	//////////////////////////Attributs
private String code_article;
private String description;
private double prix;


  ////////////////////////////Constructeur
public Article()
{
	this.code_article = "";
	this.description = "";
	this.prix=0.0;
}

public Article(String code_article,String description,double prix)
{
	this.code_article=code_article;
	this.description = description;
	this.prix=prix;
}

public String getCode_article() {
	return code_article;
}

public void setCode_article(String code_article) {
	this.code_article = code_article;
}

public String getDescription() {
	return description;
}

public void setDescription(String description) {
	this.description = description;
}

public double getPrix() {
	return prix;
}

public void setPrix(double prix) {
	this.prix = prix;
}







}

