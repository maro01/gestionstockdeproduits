package WindowBuilder.poo;

public class Stock {

	private Article article;
	private Magasin magasin;
	private double qte;

	//////////////////////////// Constructeur
	public Stock() {
		Article article = new Article();
		Magasin magasin = new Magasin();
		qte = 0.0;
	}

	public Stock(Article article, Magasin magasin, double qte) {
		this.article = article;
		this.magasin = magasin;
		this.qte = qte;
	}

	////////////////////////// Getters&Setters
	public double getQte() {
		return this.qte;
	}

	public Article getArticle() {
		return this.article;
	}

	public Magasin getMagasin() {
		return this.magasin;
	}

	public void setQte(double qte) {
		this.qte = qte;
	}

}

