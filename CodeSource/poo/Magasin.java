package WindowBuilder.poo;


public class Magasin {
	
        //////////////////////////Attributs
private String code_magasin;
private String  description;


       ////////////////////////////Constructeur
public Magasin()
{
	this.code_magasin="";
	this.description="";
}
public Magasin(String code_magasin,String description)
{
	this.code_magasin=code_magasin;
	this.description=description;
}


      ////////////////////////////Getters & Setters
public String getCode_magasin() {
	return code_magasin;
}
public void setCode_magasin(String code_magasin) {
	this.code_magasin = code_magasin;
}
public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}




}

