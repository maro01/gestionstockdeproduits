# GestionStockdeProduits

"MyStock" est une application Desktop d'amateur pour la gestion des stocks de produits développée en Java par la bibliothèque graphique JSwing
Cette application requiert une authentification et puis un formulaire à remplir par les informations du stock et puis le sauvegarde
des informations remplies dans un fichier .txt sous un nom de votre choix, mis  dans un dossier "sauvegarde d'application".

Dans le dossier "sauvegarde d'application" il existe un dossier caché qui s'appelle "sécurité" qui contient 3 fichiers .txt cachés "Data.txt" , "user.txt" et "password.txt" , ces 3 fichiers contiennet les informations d'utilisateur qui servent à déterminer l'utilisateur dans la page de connexion.
